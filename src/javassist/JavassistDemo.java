package javassist;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 *
 * @author Damian Terlecki
 */

//http://jboss-javassist.github.io/javassist/html/

public class JavassistDemo {

    /**
     * @param args the command line arguments
     * @throws java.lang.IllegalAccessException
     * @throws javassist.CannotCompileException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws java.lang.NoSuchMethodException
     * @throws javassist.NotFoundException
     */
    public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, CannotCompileException, NotFoundException {
        CtClass ctClass = ClassPool.getDefault().get("javassist.HelloWorld");
        CtMethod newMethod = CtNewMethod.make("public void printSecretPassword() { System.out.println(\"PTAKI LATAJA KLUCZEM\"); }", ctClass);
        ctClass.addMethod(newMethod);
        Class helloWorld = ctClass.toClass();
        Method printSecretPassword = HelloWorld.class.getDeclaredMethod("printSecretPassword", null);
        printSecretPassword.invoke(new HelloWorld(), null);
    }

}
